<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Spring Boot with Apache Tiles</title>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.js"></script>
<!--     	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.js"></script> -->
<!--     	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-sanitize.js"></script> -->
    	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-route.js"></script>
    	<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
    	<script src="/static/app/mainApp.js"></script>
    	<link href="/static/css/bootstrap.min.css" rel="stylesheet">
    	<link href="/static/css/app/main.css" rel="stylesheet">
	</head>
	<body>
		<div>
			<div class="container" >
				<tiles:insertAttribute name="header" />
				<tiles:insertAttribute name="body" />
				<tiles:insertAttribute name="footer" />
			</div>
		</div>
	</body>
</html>