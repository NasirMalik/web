<div class="modal-header">{{employeeFormHeader}}</div>

<div class="modal-body">
	<form role="form" class="form-horizontal">

		<fieldset>
			<legend>Personal Information</legend>

			<div class="form-group">
				<label for="fullName" class="col-sm-3 control-label">Name</label>
				<div class="col-sm-9">
					<input type="text" id="fullName" name="fullName"
						ng-model="person.fullName" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="country" class="col-sm-3 control-label">Country</label>
				<div class="col-sm-9">
					<select id="country" name="country" ng-model="person.country"
						class="form-control">
						<optgroup label="Local">
							<option id="AU">Australia</option>
							<option id="NZ">New Zealand</option>
						</optgroup>
						<optgroup label="Immigrant">
							<option id="PK">Pakistan</option>
							<option id="IN">India</option>
						</optgroup>
					</select>
				</div>
			</div>

		</fieldset>

		<fieldset>
			<legend>Employment Details</legend>

			<div class="form-group">
				<label for="company" class="col-sm-3 control-label">Company
					Name</label>
				<div class="col-sm-9">
					<input type="text" id="company" name="company" ng-model="person.company" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="department" class="col-sm-3 control-label">Department</label>
				<div class="col-sm-9">
					<select id="department" name="department"
						ng-model="person.department" class="form-control"
						ng-options="key as value for (key, value) in departments">
					</select>
				</div>
			</div>

		</fieldset>
	</form>
</div>

<div class="modal-footer">
	<div class="col-sm-offset-3 col-sm-9">
		<input type="submit" value="Cancel" class="btn btn-default" ng-click="cancel()"> 
		<input type="submit" value="{{saveCaption}}" class="btn btn-danger" ng-click="submit()">
	</div>
</div>