{{message}}
<div>
	<input type="button" class="btn btn-primary" value="Add New"
		ng-click="openEmployeeForm()">
</div>

<div>
	<table class="table table-striped">
		<thead>
			<th>Name</th>
			<th>Country</th>
			<th>Company</th>
			<th>Department</th>
		</thead>
		<tr ng-repeat="emp in employees" >
			<td>{{emp.fullName}}</td>
			<td>{{emp.country}}</td>
			<td>{{emp.company}}</td>
			<td>{{emp.department}}</td>
		</tr>
	</table>
</div>

<div>
	<input type="button" class="btn btn-danger" value="Delete Selected"
		ng-click="deleteEmployees()">
</div>