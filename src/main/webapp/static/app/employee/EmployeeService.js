mainApp.factory('empService', 
	function($http){
	
		var empService = {
				
				getDepartments: function(){
					var promise = $http.get("/data/departments").then(function(response){
						return response.data;
					})
					return promise;
				},				
				
				getPerson: function(){
					var person = {
						fullName: '',
						country: '',
						company: '',
						department: ''			
					}
					return person;
				},
				
				getEmployees: function() {
					var promise = $http.get("/data/employees").then(function(response){
						return response.data;
					})
					return promise;
				},
				
				saveEmployee: function(person) {
					return $http.post("/data/saveEmployee/" + person.fullName + "/" + person.country + "/" + person.company + "/" + person.department);
				},
				
				updateEmployee: function(id) {
					return $http.get("/data/saveEmployee/" + id);
				},
				
				deleteEmployee: function(id) {
					return $http.get("/data/deleteEmployee/" + id);
				}
				
		}
		
		return empService;
		
	}
);
