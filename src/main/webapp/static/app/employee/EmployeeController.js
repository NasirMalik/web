mainApp.controller('empController', function($scope, $uibModal, empService) {

	$scope.message = 'From EmpController';
	$scope.person = empService.getPerson();
	
	empService.getDepartments().then(function(data) {
		$scope.departments = data;
	});
	
	empService.getEmployees().then(function(data) {
		$scope.employees = data;
	});
	
	$scope.openEmployeeForm = function(id) {
		if (id){
			$scope.employeeFormHeader = 'Enter New Employee';
			$scope.saveCaption = 'Save Employee';
		}
		else{
			$scope.employeeFormHeader = 'Update Existing Employee';
			$scope.saveCaption = 'Update Employee';
		}
		return $scope.popup = $uibModal.open({
			templateUrl: '/employee',
			controller: 'empController',
			scope: $scope
		});
	}

	$scope.submit = function() {
		empService.saveEmployee($scope.person).then(function(response){
			if (response.data){
				$scope.employees.push(response.data);
				$scope.$apply();
				$scope.popup.close('saved!');
			}
		});
	}

	$scope.cancel = function() {
		$scope.popup.close('canceled!');
	}

	$scope.deleteEmployees = function() {
		alert('Not Implemented!');
	}
	
});


