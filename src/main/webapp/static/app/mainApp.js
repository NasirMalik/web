var mainApp = angular.module('mainApp', ['ui.bootstrap', 'ngRoute']);

mainApp.config(function($routeProvider){
	$routeProvider
	.when('/home', {
		templateUrl: '/home',
		controller: 'homeController'
	})
	.when('/listEmployees', {
		templateUrl: '/listEmployees',
		controller: 'empController'
	})
	.otherwise({
		redirectTo: '/home'
	});
});