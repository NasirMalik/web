package com.nasir.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nasir.model.Employee;
import com.nasir.model.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	public List<Employee> getEmployees(){
		List<Employee> employeeList = (List<Employee>) employeeRepository.findAll();
		return employeeList;
	}
		
	public Employee saveEmployee(Employee employee){
		return employeeRepository.save(employee);
	}

}
