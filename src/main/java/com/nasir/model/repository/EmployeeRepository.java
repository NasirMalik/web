package com.nasir.model.repository;

import org.springframework.data.repository.CrudRepository;

import com.nasir.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	
	
}
