package com.nasir.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nasir.model.Employee;
import com.nasir.service.EmployeeService;

@RestController
@RequestMapping("/data")
public class DataController {
	
	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "/departments", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getDepartments() {
		Map<String, String> depts = new HashMap<String, String>();
		depts.put("IT", "Information Technology");
		depts.put("SW", "Software");
		depts.put("SYS", "Systems");
		depts.put("MKS", "Marketing & Sales");
		return depts;
	}
	
	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public @ResponseBody List<Employee> getEmployees() {
		return employeeService.getEmployees();
	}
	
	@RequestMapping(value = "/saveEmployee/{fullName}/{country}/{company}/{department}", method = RequestMethod.POST)
	public @ResponseBody Employee saveEmployee(
			@PathVariable String fullName,
			@PathVariable String country,
			@PathVariable String company,
			@PathVariable String department) {
		Employee emp = new Employee();
		emp.setFullName(fullName);
		emp.setCompany(company);
		emp.setCountry(country);
		emp.setDepartment(department);
		return employeeService.saveEmployee(emp);
	}
	
}